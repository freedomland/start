inventoryHelper = require("inventoryHelper")
logicHandler = require("logicHandler")
CommonFunctions = require("CommonFunctions")
mpWidgets = require("mpWidgets")
mpMusic = require("mpMusic")
activateInterface = require("activateInterface")

require("color")
require("config")
require("tr_items")
require("betterLuaRandom")

Methods = {}

local classes = {
	["acrobat"] = {"Вор", "Охотник"},
	["agent"] = {"Охотник", "Воин/атака", "Воин/защита", "Вор", "Маг/атака", "Маг/защита"},
	["archer"] = {"Вор", "Охотник"},
	["assassin"] = {"Вор", "Охотник"},
	["barbarian"] = {"Воин/атака", "Воин/защита"},
	["bard"] = {"Вор"},
	["battlemage"] = {"Воин/атака", "Воин/защита", "Маг/атака"},
	["crusader"] = {"Маг/защита", "Воин/защита", "Воин/атака"},
	["healer"] = {"Маг/защита"},
	["knight"] = {"Воин/защита", "Воин/атака", "Вор"},
	["mage"] = {"Маг/защита", "Маг/атака"},
	["monk"] = {"Вор", "Охотник", "Воин/атака", "Воин/защита", "Маг/атака", "Маг/защита"},
	["nightblade"] = {"Вор", "Охотник", "Воин/атака", "Воин/защита", "Маг/атака", "Маг/защита"},
	["pilgrim"] = {"Охотник", "Воин/атака", "Воин/защита", "Вор"},
	["rouge"] = {"Вор"},
	["scout"] = {"Охотник", "Вор"},
	["sorcerer"] = {"Маг/атака", "Маг/защита"},
	["spellsword"] = {"Воин/атака", "Воин/защита", "Маг/атака", "Маг/защита"},
	["thief"] = {"Вор", "Охотник"},
	["warrior"] = {"Воин/атака", "Воин/защита"},
	["witchhunter"] = {"Охотник", "Маг/защита"}
}
	
local reward = {
	["Маг/защита"] = {
		{"p_cure_common_s", 1},
		{"p_restore_health_c", 5},
		{"p_fortify_health_c", 3},
		{"p_fortify_magicka_c", 1},
		{"wooden staff", 1},
		{"iron dagger", 1},
		{"common_robe_02_r", 1},
		{"gold_001", 100}
	},
		
	["Маг/атака"] = {
		{"p_restore_health_s", 3},
		{"p_fortify_magicka_c", 3},
		{"p_fortify_intelligence_c", 2},
		{"wooden staff", 1},
		{"iron dagger", 1},
		{"common_robe_02_r", 1},
		{"gold_001", 100}
	},
		
	["Воин/атака"] = {
		{"iron_cuirass", 1},
		{"iron boots", 1},
		{"iron_pauldron_left", 1},
		{"iron_gauntlet_right", 1},
		{"iron warhammer", "iron longsword", "iron broadsword", 1},
		{"p_restore_health_s", 1},
		{"gold_001", 100}
	},
		
	["Воин/защита"] = {
		{"iron_cuirass", 1},
		{"iron boots", 1},                                                                                                                                                                                                                         
		{"iron_pauldron_right", 1},
		{"iron_gauntlet_left", 1},
		{"iron_pauldron_left", 1},
		{"iron_gauntlet_right", 1},
		{"iron_shield", 1},
		{"iron broadsword", "iron shortsword", 1},
		{"p_restore_health_s", 3},                                              
		{"gold_001", 100}
	},
		
	["Охотник"] = {
		{"chitin short bow", 1},
		{"fur_cuirass", 1},
		{"chitin arrow", 50},
		{"fur_greaves", 1},
		{"fur_boots", 1},
		{"ingred_kagouti_hide_01", 15},
		{"gold_001", 100}
	},
		
	["Вор"] = {
		{"pick_apprentice_01", 5},
		{"probe_apprentice_01", 3},
		{"steel dagger", 1},                                                       
		{"gold_001", 250}
	}
}

local weapons = {
	["bluntweapon"] = {"iron club", "chitin club", "firebite club", "spiked club", "steel club", "steel flamemace", "steel mace", "steel shardmace", "steel sparkmace", "steel vipermace", "iron flamemace", "iron mace", "iron shardmace", "iron sparkmace", "shockbite mace", "steel warhammer", "warhammer of wounds", "iron warhammer", "shockbite warhammer"},
	["axe"] = {"silver viperaxe", "steel flameaxe", "steel shardaxe", "steel sparkaxe", "steel viperaxe", "silver shardaxe", "chitin war axe", "firebite war axe", "iron battle axe", "iron sparkaxe", "iron viperaxe", "iron war axe", "nordic battle axe", "orcish battle axe", "shockbite battle axe", "shockbite war axe", "silver flameaxe", "silver sparkaxe", "battle axe of wounds", "steel axe", "steel battle axe", "steel war axe", "war axe of wounds"},
	["longblade"] = {"steel broadsword", "steel claymore", "steel katana", "steel longsword", "imperial broadsword", "iron longsword", "iron saber", "iron claymore", "nordic broadsword", "nordic claymore", "silver claymore"},
	["shortblade"] = {"steel flameblade", "cruel shardblade", "cruel sparkblade", "cruel viperblade", "steel shardblade", "steel viperblade", "steel sparkblade", "wild flameblade", "wild shardblade", "wild sparkblade", "wild viperblade", "silver viperblade", "cruel flameblade", "iron shardblade", "iron broadsword", "iron flameblade", "iron sparkblade", "iron viperblade", "iron wakizashi", "silver flameblade", "silver shardblade", "silver sparkblade", "iron dagger", "iron shardsword", "iron shortsword", "iron flamesword", "iron sparksword", "iron tanto", "iron vipersword", "silver dagger", "silver flamesword", "silver longsword", "silver shardsword", "silver shortsword", "silver sparksword", "imperial shortsword", "chargen dagger", "chitin dagger", "chitin shortsword", "cruel flamesword", "cruel shardsword", "cruel sparksword", "cruel vipersword", "firebite dagger", "firebite sword", "silver vipersword", "steel dagger", "steel dai-katana", "steel firesword", "steel flamesword", "steel frostsword", "steel jinksword", "steel tanto", "steel wakizashi", "steel poisonsword", "steel shardsword", "steel shortsword", "steel sparksword", "steel stormsword", "steel vipersword", "wild flamesword", "wild shardsword", "wild sparksword", "wild vipersword"},
	["spear"] = {"chitin spear", "iron spear", "steel spear", "shockbite halberd", "iron halberd", "steel halberd", "silver staff", "silver staff of reckoning", "steel staff", "steel staff of chastening", "steel staff of divine judgement", "steel staff of peace", "steel staff of shaming", "steel staff of the ancestors", "steel staff of war",
		"wooden staff", "wooden staff of chastening", "wooden staff of divine", "wooden staff of peace", "wooden staff of shaming", "wooden staff of war"}
}

function Methods.OnPlayerChargenEnded(pid)
	CommonFunctions.PlayBIK(pid, "fl_start")
	CommonFunctions.FadeOut(pid, 0)
	CommonFunctions.TM(pid)
	CommonFunctions.PlayerControlls(pid, false)

	for i = 1,8 do
		if LoadedCells["Ship0" .. tostring(i)] == nil then
			CommonFunctions.COC(pid, "Ship0" .. i, {posX = 4200.530273, posY = 3924.396729, posZ = 12048.669922})		
			break
		end
	end
	
	CommonFunctions.FadeIn(pid, 3)
	
	mpMusic.StreamMusic(pid)
	
	local msg = "Вы пробудились от громкого шума и криков. Кажется на верхней палубе что-то происходит."
	
	timerApiEx.CreateTimer("MSG" .. pid, time.seconds(3), 
											function() 
												CommonFunctions.TM(pid) 
												CommonFunctions.PlayerControlls(pid, true) 
												mpWidgets.MessageBox(pid, msg, "NORMAL")
											end)
	timerApiEx.StartTimer("MSG" .. pid)
end

function Turtorial(pid)
	local msg = ""
	
	msg = color.White .. msg .. "Этот турториал поможет вам выбрать стартовый набор. Прочитайте его внимательно, чтобы не было вопросов.\n\n"
	msg = msg .. "Стартовый набор зависит от класса, который вы выбрали. Зачем это нужно? Стартовый набор это ваше стартовое снаряжение и оружие.\n\n"
	msg = msg .. "Как вы уже могли заметить наш сервер не совсем обычный и тут никто не говорит вам прямо что вы должны спасти мир. Вы обычный человек, который #F84463может стать" .. color.White .. " Нереварином как изначально предполагалось разработчиками Morrowind.\n\n"
	msg = msg .. "Обратите внимание, что после получения стартового набора его уже #F84463невозможно будет изменить!\n\n" .. color.White
	msg = msg .. "Следующее меню покажет вам набор кнопок, нажав на которые вы можете посмотреть что предлагает этот стартовый набор и принять (или отклонить) этот набор."
	
	local menuId = config.customMenuIds.FLSTurt
	
	mpWidgets.New(menuId, msg)
	mpWidgets.AddButton(menuId, 0, "Далее", function() StartEquip(pid) end)
	mpWidgets.Show(pid, menuId, 1)
end

function StartEquip(pid)
	local menuId = config.customMenuIds.FLSEquip

	mpWidgets.New(menuId,  "Выберите ваш стартовый набор:")

	local class = Players[pid].data.character.class
	
	for i, t in ipairs(classes[class]) do
		mpWidgets.AddButton(menuId, i-1, t, function() PlayerSelectEquip(pid, i) end)
	end
	
	mpWidgets.Show(pid, menuId, 1)
end

function PlayerSelectEquip(pid, num)
	local class = Players[pid].data.character.class
	local kit = classes[class][num]
	local msg = ""
	local num = nil
			
	for i, t in pairs(reward[kit]) do
		if #t > 2 then
			num = getRandNum(1, #t-1)
			msg = msg .. tr_items(t[num]) .. " (x" .. t[#t] .. ")\n"
		else
			msg = msg .. tr_items(t[1]) .. " (x" .. t[2] .. ")\n"
		end
	end
	
	local menuId = config.customMenuIds.FLSKit
	
	mpWidgets.New(menuId, "Выбрав " .. kit:lower() .. " вы получите:\n\n" .. msg)
	mpWidgets.AddButton(menuId, 0, "Выбрать другой набор", function() StartEquip(pid) end)
	mpWidgets.AddButton(menuId, 1, "Взять этот набор", function() AddToInventory(pid, kit, num) end)
	mpWidgets.Show(pid, menuId, 1)
end

function AddToInventory(pid, kit, num)
	for i, t in pairs(reward[kit]) do
		if #t > 2 and num ~= nil then
			local num = getRandNum(1, #t-1)
			CommonFunctions.AddItem(pid, t[num], t[#t])
		else
			CommonFunctions.AddItem(pid, t[1], t[2])
		end
	end
	
	mpWidgets.MessageBox(pid, "Набор был добавлен в ваш инвентарь.", "NORMAL")
	
	Players[pid].data.customVariables.startState = -1
end

function Methods.OnCellChanged(pid, cell)
	if Players[pid].data.customVariables.startState == -1 then
		return
	end
	
	if Players[pid].data.customVariables.startState == nil and cell == "-2, -9" then
		CommonFunctions.RemoveItem(pid, Players[pid].data.customVariables.startWeapon, 1)
		Turtorial(pid)
	end
	
	if cell == "Seyda Neen, Census and Excise Office" then
		local invInd = inventoryHelper.getItemIndex(Players[pid].data.inventory, "bk_a1_1_caiuspackage", -1, -1, "")
		if invInd == nil then
			CommonFunctions.AddItem(pid, "bk_a1_1_caiuspackage", 1)
		end
	end
end

function FindLargerSkill(pid)
	local bluntweapon = Players[pid].data.skills.Bluntweapon
	local axe = Players[pid].data.skills.Axe
	local longblade = Players[pid].data.skills.Longblade
	local shortblade = Players[pid].data.skills.Shortblade
	local spear = Players[pid].data.skills.Spear
	local prev = 0	
	local skill = ""
		
	local largernum = {bluntweapon, axe, longblade, shortblade, spear}
		
	for i, t in pairs(largernum) do
		if prev < t then
			prev = t
				
			if i == 1 then
				skill = "bluntweapon"
			elseif i == 2 then
				skill = "axe"
			elseif i == 3 then
				skill = "longblade"
			elseif i == 4 then
				skill = "shortblade"
			elseif i == 5 then
				skill = "spear"
			end
		end
	end
	
	return skill
end

function OnCapDoorActivate(pid, cellDescription, objectRefId, refNum, mpNum)
	if cellDescription == "-1, -9" then
		mpWidgets.MessageBox(pid, "Тут вам делать больше нечего.", "INFO")
		return
	end

	mpMusic.StopMusic(pid)
	CommonFunctions.PlayBIK(pid, "fl_start2")
	CommonFunctions.COC(pid, "-2, -9", {posX = -9110.171875, posY = -72942.898438, posZ = 89.500000})
		
	local retriveCell = cellDescription:split(" ")[3]
		
	logicHandler.UnloadCellForPlayer(pid, "Ship" .. retriveCell)
	logicHandler.UnloadCellForPlayer(pid, cellDescription)
		
	LoadedCells["Ship" .. retriveCell] = nil
	LoadedCells[cellDescription] = nil
		
	os.remove(os.getenv("MOD_DIR") .. "/cell/" .. "Ship" .. retriveCell .. ".json")
	os.remove(os.getenv("MOD_DIR") .. "/cell/" .. cellDescription .. ".json")
		
	CommonFunctions.FadeIn(pid, 1)
end

function OnDeadBodyActivate(pid, cellDescription, objectRefId, refNum, mpNum)
	if Players[pid].data.customVariables.startWeapon ~= nil then
		return
	end

	local rw = getRandValue(weapons[FindLargerSkill(pid)])
	local msg = ""
		
	msg = msg .. tr_items(rw) .. "\n"
	msg = msg .. tr_items("p_restore_health_c") .. " (x5)\n"
	msg = msg .. tr_items("p_restore_fatigue_c") .. " (x5)\n"
	
	CommonFunctions.AddItem(pid, rw, 1)
	CommonFunctions.AddItem(pid, "p_restore_health_c", 5)
	CommonFunctions.AddItem(pid, "p_restore_fatigue_c", 5)
	
	Players[pid].data.customVariables.startWeapon = rw
	
	mpWidgets.MessageBox(pid, "С трупа вы взяли:\n\n" .. msg, "NORMAL")
end

function Methods.Init()
	activateInterface.AddRefid("fm_cap_door_01", OnCapDoorActivate)
	activateInterface.AddRefid("fm_dead_body", OnDeadBodyActivate)
end

return Methods
